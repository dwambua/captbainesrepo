/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package mygame;

import com.jme3.app.Application;
import com.jme3.app.state.AbstractAppState;
import com.jme3.app.state.AppStateManager;
import com.jme3.input.event.MouseButtonEvent;
import com.jme3.math.Vector2f;
import tonegod.gui.controls.buttons.ButtonAdapter;
import tonegod.gui.controls.windows.Panel;
import tonegod.gui.core.Screen;

/**
 * Manages all the gui stuff when the game is paused
 * @author dennis
 */
public class PauseScreen extends AbstractAppState {
    private Panel win;
    private Screen screen;
    private Main m;
    private boolean start = false;
    private ButtonAdapter resume;
    private ButtonAdapter menu;

    public PauseScreen(Screen screen, Main m){
        this.screen = screen;
        this.m = m;
    }
    
     private void resumeGame(){
         
        start = true;
        
    }
    
    @Override
    public void initialize(AppStateManager stateManager, Application app) {
        super.initialize(stateManager, app);
        //TODO: initialize your AppState, e.g. attach spatials to rootNode
        //this is called on the OpenGL thread after the AppState has been attached
        win = new Panel(screen, "win", new Vector2f((screen.getWidth() / 2)-(350/2),
                screen.getHeight() / 3), new Vector2f(350, 210));

        // create button and add to window
        resume = new ButtonAdapter(screen, new Vector2f((win.getWidth()/2)-100, 10),
                new Vector2f(200, 50)) {
            @Override
            public void onButtonMouseLeftUp(MouseButtonEvent evt, boolean toggled) {
               resumeGame();
               m.playButtonSound();
            }
        };
        menu = new ButtonAdapter(screen, new Vector2f((win.getWidth()/2)-100, 80),
                new Vector2f(200, 50)) {
            @Override
            public void onButtonMouseLeftUp(MouseButtonEvent evt, boolean toggled) {
                m.reinit();
                
            }
        };
        

        resume.setButtonIcon(200, 50, "Interface/RESUME.png");
        menu.setButtonIcon(200, 50, "Interface/MENU.png");
        

        
        win.setIsMovable(false);
        win.setIsResizable(false);
        win.addChild(resume);
        win.addChild(menu);


        // Add window to the screen
        screen.addElement(win);
    }
    
    @Override
    public void update(float tpf) {
        //TODO: implement behavior during runtime
        
        if (start == true){
            
            m.unpauseGame();
        }
    }
    
    @Override
    public void cleanup() {
        super.cleanup();
        //TODO: clean up what you initialized in the initialize method,
        //e.g. remove all spatials from rootNode
        //this is called on the OpenGL thread after the AppState has been detached
       start = false;
        screen.removeElement(win);
    }
}
