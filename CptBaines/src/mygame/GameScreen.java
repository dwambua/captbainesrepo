/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package mygame;

import com.jme3.app.Application;
import com.jme3.app.state.AbstractAppState;
import com.jme3.app.state.AppStateManager;
import com.jme3.input.event.MouseButtonEvent;
import com.jme3.math.ColorRGBA;
import com.jme3.math.FastMath;
import com.jme3.math.Quaternion;
import com.jme3.math.Vector2f;
import com.jme3.math.Vector3f;
import tonegod.gui.controls.buttons.ButtonAdapter;
import tonegod.gui.controls.extras.android.Joystick;
import tonegod.gui.controls.windows.Panel;
import tonegod.gui.core.Screen;

/**
 *Manages all the gui stuff during the game
 * @author dennis
 */
public class GameScreen extends AbstractAppState {

    private Screen screen;
    private Main m;
    private boolean pause = false;
    private Panel score;
    private ButtonAdapter pauseA;
    private Joystick direction;
    private Joystick speed;

    public GameScreen(Screen screen, Main m) {
        this.screen = screen;
        this.m = m;
    }

    @Override
    public void initialize(AppStateManager stateManager, Application app) {
        super.initialize(stateManager, app);
        //TODO: initialize your AppState, e.g. attach spatials to rootNode
        //this is called on the OpenGL thread after the AppState has been attached

        score = new Panel(screen, new Vector2f(5, 5), new Vector2f(70, 40));
        score.setTextPosition(7, 7);
        score.setText("Score");
        score.setFont("Interface/Fonts/DroidSerif.fnt");
        score.setFontColor(ColorRGBA.Green);
        score.setIsMovable(false);
        score.setIsResizable(false);



        pauseA = new ButtonAdapter(screen, new Vector2f(screen.getWidth() - 60, 5),
                new Vector2f(50, 50)) {
            @Override
            public void onButtonMouseLeftUp(MouseButtonEvent evt, boolean toggled) {
                pauseGame();
            }
        };
        pauseA.setButtonIcon(50, 50, "Interface/pause.png");
        screen.addElement(score);
        screen.addElement(pauseA);
        initTest();
        
        loadTest();
    }

    @Override
    public void update(float tpf) {
        //TODO: implement behavior during runtime
        if (pause == true) {
            m.pauseGame();
        }
        score.setText(String.valueOf(m.getScore()));
    }

    private void pauseGame() {
        pause = true;
    }

    @Override
    public void cleanup() {
        super.cleanup();
        //TODO: clean up what you initialized in the initialize method,
        //e.g. remove all spatials from rootNode
        //this is called on the OpenGL thread after the AppState has been detached
        pause = false;
        screen.removeElement(pauseA);
        screen.removeElement(score);
        // Remove joysticks from screen
        screen.removeElement(speed);
        screen.removeElement(direction);
        
    }
    private void loadTest() {
		// Add joysticks to screen
		screen.addElement(speed);
		screen.addElement(direction);
	}

    private void initTest() {
        //////////////////////
        // Create Joystick 1
        //////////////////////
        speed = new Joystick(screen,
                new Vector2f(10, screen.getHeight() - 138),
                128) {
            @Override
            public void onUpdate(float tpf, float deltaX, float deltaY) {
                
                
            }
        };

        //////////////////////
        // Create Joystick 2
        //////////////////////
        direction = new Joystick(screen,
                new Vector2f(screen.getWidth() - 138, screen.getHeight() - 138),
                128) {
            @Override
            public void onUpdate(float tpf, float deltaX, float deltaY) {
                //float x = m.getBankAngle()+(-deltaX*tpf*1000);                
                float y = m.getPitchAngle()+(deltaY*tpf*200);
                //m.setBankAngel(x);
                m.setPitchAngle(y);
            }
        };
    }
}
