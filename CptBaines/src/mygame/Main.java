package mygame;

import com.jme3.app.SimpleApplication;
import com.jme3.audio.AudioNode;
import com.jme3.bounding.BoundingVolume;
import com.jme3.collision.CollisionResults;
import com.jme3.font.BitmapText;
import com.jme3.input.Joystick;
import com.jme3.input.JoystickAxis;
import com.jme3.input.SensorJoystickAxis;
import com.jme3.input.controls.AnalogListener;
import com.jme3.light.PointLight;
import com.jme3.material.Material;
import com.jme3.math.ColorRGBA;
import com.jme3.math.FastMath;
import com.jme3.math.Quaternion;
import com.jme3.math.Vector3f;

import com.jme3.renderer.RenderManager;
import com.jme3.scene.CameraNode;
import com.jme3.scene.Geometry;
import com.jme3.scene.Mesh;
import com.jme3.scene.Node;
import com.jme3.scene.Spatial;
import com.jme3.scene.VertexBuffer;
import com.jme3.scene.control.CameraControl;
import com.jme3.scene.shape.Box;
import com.jme3.terrain.geomipmap.TerrainQuad;
import com.jme3.terrain.heightmap.AbstractHeightMap;
import com.jme3.terrain.heightmap.ImageBasedHeightMap;
import com.jme3.texture.Texture;
import com.jme3.util.BufferUtils;
import com.jme3.util.IntMap;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import tonegod.gui.core.Screen;

/**
 * The main classs that loads everything and manages the game
 *
 * @author dennis
 */
public class Main extends SimpleApplication implements AnalogListener {

    private static final Logger logger = Logger.getLogger(Main.class.getName());
    Spatial plane; //plane model
    Node planeNode; //Node for collision detection
    float bankAngle = 0; //init bank angle
    float pitchAngle = 0; //init pitch angle
    Quaternion lookForward = new Quaternion(); //Rotation for aligning the plane correctly
    Quaternion bank = new Quaternion(); //Rotation for banking
    Quaternion pitch = new Quaternion(); //Rotation for pitching
    // mappings used for onAnalog
    private final String ORIENTATION_X_PLUS = "Orientation_X_Plus";
    private final String ORIENTATION_X_MINUS = "Orientation_X_Minus";
    private final String ORIENTATION_Y_PLUS = "Orientation_Y_Plus";
    private final String ORIENTATION_Y_MINUS = "Orientation_Y_Minus";
    private final String ORIENTATION_Z_PLUS = "Orientation_Z_Plus";
    private final String ORIENTATION_Z_MINUS = "Orientation_Z_Minus";
    private IntMap<Joystick> joystickMap = new IntMap<Joystick>();
    private float rotationSpeedX = 5.0f;
    private float rotationSpeedY = 20.0f;
    private float maxBank = 60.0f;
    private float maxPitch = 10.0f;
    private float planeSpeed = -5.0f;
    private float turnSpeed = 5.0f;
    // toggle to enable controlling geometry rotation
    boolean enableGeometryRotation = true;
    public boolean running = false;
    private Geometry target;
    private Node maze; //Holds all the targets in a scene
    private BoundingVolume planeSpace; //Bounding volume for collision detection
    private int score = 0;
    public AudioNode audio_rotor; //plane rotor sound
    private AudioNode audio_intro;
    public AudioNode audio_pause;
    public AudioNode audio_start;
    private AudioNode audio_target;
    private TerrainQuad terrain;
    Material matRock;
    Material matWire;
    boolean wireframe = false;
    boolean triPlanar = false;
    protected BitmapText hintText;
    PointLight pl;
    Geometry lightMdl;
    private float grassScale = 64;
    private float dirtScale = 16;
    private float rockScale = 128;
    private StartScreen ss;
    private GameScreen gm;
    private PauseScreen ps;
    boolean start = false;
    boolean paused = false;
    private Screen screen;
    
    private boolean soundEnabled = true;
    private boolean acceleratorEnabled = true; 

    public static void main(String[] args) {
        Logger.getLogger("com.jme3").setLevel(Level.ALL);
        Main app = new Main();
        app.start();
    }

    @Override
    public void simpleInitApp() {

        setDisplayStatView(false);
        flyCam.setEnabled(false);
        inputManager.setCursorVisible(true);
        inputManager.setSimulateMouse(false);
        
        audio_intro = new AudioNode(assetManager, "Sounds/intro.wav", false);
        audio_intro.setPositional(false);
        audio_intro.setLooping(true);
        audio_intro.setVolume(2);
        
        rootNode.attachChild(audio_intro);
        if(soundEnabled)
            audio_intro.play();

        screen = new Screen(this);//, "tonegod/gui/style/def/style_map.xml");
        screen.setUseCustomCursors(true);
        screen.setUseMultiTouch(true);

        //screen.setUseKeyboardIcons(true);
        screen.setUse3DSceneSupport(true);


        guiNode.addControl(screen);
        ss = new StartScreen(screen, this);
        gm = new GameScreen(screen, this);
        ps = new PauseScreen(screen, this);

        stateManager.attach(ss);
         
        initAll();

    }

    private void createMaze() {
        maze = new Node("Maze");
        maze.attachChild(createTarget("Target 1", ColorRGBA.Green, new Vector3f(0, -5, -20)));
        maze.attachChild(createTarget("Target 2", ColorRGBA.Green, new Vector3f(-10, -5, -40)));
        maze.attachChild(createTarget("Target 3", ColorRGBA.Green, new Vector3f(0, -5, -60)));
        maze.attachChild(createTarget("Target 4", ColorRGBA.Green, new Vector3f(10, -5, -80)));
        maze.attachChild(createTarget("Target 5", ColorRGBA.Green, new Vector3f(0, -5, -100)));

        rootNode.attachChild(maze);
    }

    private Spatial createTarget(String name, ColorRGBA color, Vector3f location) {
        Box b = new Box(1, 1, 1);
        Spatial target = new Geometry(name, b);
        Material matGreen = new Material(assetManager, "Common/MatDefs/Misc/Unshaded.j3md");
        matGreen.preload(renderManager);
        matGreen.setColor("Color", ColorRGBA.Green);
        target.setMaterial(matGreen);

        target.setLocalScale(2, 2, 0.2f);
        target.setLocalTranslation(location);
        return target;
    }

    private void initAll() {

        initSky();
        initSound();
        initPlane();
        initCamera();
        initTerrain();      
        initJoystick();
        createMaze();
        
    }
    
    
    public void reinit(){
        rootNode.detachChild(terrain);
        rootNode.detachChild(planeNode);
        rootNode.detachChild(maze);
        score = 0;
        
        initAll();
        paused = false;
        stateManager.detach(ps);
        stateManager.attach(ss);
    }
    

    public void startGame() {
        start = true;
        running = true;
        if(soundEnabled)
            audio_rotor.play();
        paused = false;

    }

    public void pauseGame() {
        running = false;
        audio_rotor.stop();
        paused = true;
        start = false;

    }

    public void unpauseGame() {

        paused = false;
        start = true;
        running = true;
        if(soundEnabled)
            audio_rotor.play();

    }

    public void stateChanger() {
        // System.out.println("Paused"+paused+" | Start "+start);
        if (start == true) {
            stateManager.detach(ss);

            if (stateManager.hasState(ps)) {
                stateManager.detach(ps);
            }

            stateManager.attach(gm);

        }

        if (paused == true) {
            stateManager.detach(gm);
            stateManager.attach(ps);
        }

    }

    private void initTerrain() {
        // First, we load up our textures and the heightmap texture for the terrain

        // TERRAIN TEXTURE material
        matRock = new Material(assetManager, "Common/MatDefs/Terrain/Terrain.j3md");
        matRock.setBoolean("useTriPlanarMapping", false);

        // ALPHA map (for splat textures)
        matRock.setTexture("Alpha", assetManager.loadTexture("Textures/Terrain/splat/alphamap.png"));

        // HEIGHTMAP image (for the terrain heightmap)
        Texture heightMapImage = assetManager.loadTexture("Textures/Terrain/splat/mountains128.png");

        // GRASS texture
        Texture grass = assetManager.loadTexture("Textures/Terrain/splat/grass.jpg");
        grass.setWrap(Texture.WrapMode.Repeat);
        matRock.setTexture("Tex1", grass);
        matRock.setFloat("Tex1Scale", grassScale);

        // DIRT texture
        Texture dirt = assetManager.loadTexture("Textures/Terrain/splat/dirt.jpg");
        dirt.setWrap(Texture.WrapMode.Repeat);
        matRock.setTexture("Tex2", dirt);
        matRock.setFloat("Tex2Scale", dirtScale);

        // ROCK texture
        Texture rock = assetManager.loadTexture("Textures/Terrain/splat/road.jpg");
        rock.setWrap(Texture.WrapMode.Repeat);
        matRock.setTexture("Tex3", rock);
        matRock.setFloat("Tex3Scale", rockScale);
        matRock.preload(renderManager);

        // WIREFRAME material
        matWire = new Material(assetManager, "Common/MatDefs/Misc/Unshaded.j3md");
        matWire.getAdditionalRenderState().setWireframe(true);
        matWire.setColor("Color", ColorRGBA.Green);
        matWire.preload(renderManager);

        // CREATE HEIGHTMAP
        AbstractHeightMap heightmap = null;
        try {
            heightmap = new ImageBasedHeightMap(heightMapImage.getImage(), 1f);
            heightmap.load();

        } catch (Exception e) {
            e.printStackTrace();
        }

        /*
         * Here we create the actual terrain. The tiles will be 33x33, and the total size of the
         * terrain will be 129x129. It uses the heightmap we created to generate the height values.
         */
        terrain = new TerrainQuad("terrain", 33, 129, heightmap.getHeightMap());
        //TerrainLodControl control = new TerrainLodControl(terrain, getCamera());
        //control.setLodCalculator(new DistanceLodCalculator(33, 2.7f)); // patch size, and a multiplier
        //terrain.addControl(control);
        terrain.setMaterial(matRock);
        terrain.setLocalTranslation(0, -100, -600);
        terrain.setLocalScale(8f, 0.5f, 8f);
        rootNode.attachChild(terrain);

//        DirectionalLight light = new DirectionalLight();
//        light.setDirection((new Vector3f(-0.5f, -1f, -0.5f)).normalize());
//        rootNode.addLight(light);
    }

    private void initJoystick() {

        logger.log(Level.INFO, "WAOH JOYSTICK");

        Joystick[] joysticks = inputManager.getJoysticks();
        if (joysticks == null || joysticks.length < 1) {
            logger.log(Level.INFO, "Cannot find any joysticks!");

        } else {
// Joysticks return a value of 0 to 1 based on how far the stick is
// push on the axis.  This value is then scaled based on how long
// during the frame the joystick axis has been in that position.
// If the joystick is push all the way for the whole frame,
// then the value in onAnalog is equal to tpf.
// If the joystick is push 1/2 way for the entire frame, then the
// onAnalog value is 1/2 tpf.
// Similarly, if the joystick is pushed to the maximum during a frame
// the value in onAnalog will also be scaled.
// For Android sensors, rotating the device 90deg is the same as
// pushing an actual joystick axis to the maximum.

            logger.log(Level.INFO, "Number of joysticks: {0}", joysticks.length);
            JoystickAxis axis;
            for (Joystick joystick : joysticks) {

// Get and display all axes in joystick.
                List<JoystickAxis> axes = joystick.getAxes();
                for (JoystickAxis joystickAxis : axes) {
//                    logger.log(Level.INFO, "{0} axis scan Name: {1}, LogicalId: {2}, AxisId: {3}",
//                            new Object[]{joystick.getName(), joystickAxis.getName(), joystickAxis.getLogicalId(), joystickAxis.getAxisId()});
                }

// Get specific axis based on LogicalId of the JoystickAxis
// If found, map axis
                axis = joystick.getAxis(SensorJoystickAxis.ORIENTATION_X);
                if (axis != null) {
                    axis.assignAxis(ORIENTATION_X_PLUS, ORIENTATION_X_MINUS);
                    inputManager.addListener(this, ORIENTATION_X_PLUS, ORIENTATION_X_MINUS);
//                    logger.log(Level.INFO, "Found {0} Joystick, assigning mapping for X axis: {1}, with max value: {2}",
//                            new Object[]{joystick.toString(), axis.toString(), ((SensorJoystickAxis) axis).getMaxRawValue()});

                }
                axis = joystick.getAxis(SensorJoystickAxis.ORIENTATION_Y);
                if (axis != null) {
                    axis.assignAxis(ORIENTATION_Y_PLUS, ORIENTATION_Y_MINUS);
                    inputManager.addListener(this, ORIENTATION_Y_PLUS, ORIENTATION_Y_MINUS);
//                    logger.log(Level.INFO, "Found {0} Joystick, assigning mapping for Y axis: {1}, with max value: {2}",
//                            new Object[]{joystick.toString(), axis.toString(), ((SensorJoystickAxis) axis).getMaxRawValue()});

                }
                axis = joystick.getAxis(SensorJoystickAxis.ORIENTATION_Z);
                if (axis != null) {
                    axis.assignAxis(ORIENTATION_Z_PLUS, ORIENTATION_Z_MINUS);
                    inputManager.addListener(this, ORIENTATION_Z_PLUS, ORIENTATION_Z_MINUS);
//                    logger.log(Level.INFO, "Found {0} Joystick, assigning mapping for Z axis: {1}, with max value: {2}",
//                            new Object[]{joystick.toString(), axis.toString(), ((SensorJoystickAxis) axis).getMaxRawValue()});

                }

                joystickMap.put(joystick.getJoyId(), joystick);

            }
        }


    }

    private void initSky() {
        //Spatial sky = assetManager.loadModel("Models/sky/skybox.j3o");
//        Texture west = assetManager.loadTexture("Textures/Sky/Lagoon/lagoon_west.jpg");
//        Texture east = assetManager.loadTexture("Textures/Sky/Lagoon/lagoon_east.jpg");
//        Texture north = assetManager.loadTexture("Textures/Sky/Lagoon/lagoon_north.jpg");
//        Texture south = assetManager.loadTexture("Textures/Sky/Lagoon/lagoon_south.jpg");
//        Texture up = assetManager.loadTexture("Textures/Sky/Lagoon/lagoon_up.jpg");
//        Texture down = assetManager.loadTexture("Textures/Sky/Lagoon/lagoon_down.jpg");
//
//        Spatial sky = SkyFactory.createSky(assetManager, west, east, north, south, up, down);

        //rootNode.attachChild(sky);
        //drawLineAlongZ();

        //light blue

        viewPort.setBackgroundColor(new ColorRGBA(108 / 255f, 186 / 255f, 238 / 255f, 1f));

    }

    private void initPlane() {

        lookForward.fromAngleAxis(180 * FastMath.DEG_TO_RAD, Vector3f.UNIT_Y);
        planeNode = new Node("Plane Node");
        plane = assetManager.loadModel("Models/spitfire/spitfire.j3o");

        planeNode.attachChild(plane);


        planeNode.setLocalTranslation(new Vector3f(1f, -5.6f, 0));
        planeNode.setLocalScale(0.5f);
        planeNode.setLocalRotation(lookForward);
        planeSpace = planeNode.getWorldBound();
        rootNode.attachChild(planeNode);
        


    }

    private void drawLineAlongZ() {
        Vector3f[] lineVerticies = new Vector3f[2];

        lineVerticies[0] = new Vector3f(0, -5, 10);
        lineVerticies[1] = new Vector3f(0, -5, -200);


        Mesh mesh = new Mesh();
        mesh.setMode(Mesh.Mode.Lines);



        mesh.setBuffer(VertexBuffer.Type.Position, 3, BufferUtils.createFloatBuffer(lineVerticies));


        mesh.updateBound();
        mesh.updateCounts();

        Geometry geo = new Geometry("line", mesh);
        Material mat = new Material(assetManager, "Common/MatDefs/Misc/Unshaded.j3md");
        mat.setColor("Color", ColorRGBA.Red);
        geo.setMaterial(mat);

        rootNode.attachChild(geo);
    }

    @Override
    public void simpleUpdate(float tpf) {
        //TODO: add update code
        stateChanger();

        
        if (running) {
           
            neutralise();
            checkMax();
            calculateMove(tpf);
            checkCollision();
        }

    }
    
    private void neutralise(){
        if(!acceleratorEnabled){
         if(bankAngle>0){
                bankAngle--;
            }
            if(bankAngle < 0){
                bankAngle++;
            }
        }
            if(pitchAngle > 0){
                pitchAngle--;
            }
            if(pitchAngle < 0){
                pitchAngle++;
            }
    }
    
    private void checkMax(){
        if (bankAngle < -maxBank) {
                    bankAngle = -maxBank;
             }
            if (bankAngle > maxBank) {
                    bankAngle = maxBank;
            }
            if (pitchAngle < -maxPitch) {
                pitchAngle = -maxPitch;
           }
            if (pitchAngle > maxPitch) {
                pitchAngle = maxPitch;
            }
    }
    
    private void calculateMove(float tpf){
        bank.fromAngleAxis(bankAngle * FastMath.DEG_TO_RAD, Vector3f.UNIT_Z);

            pitch.fromAngleAxis(pitchAngle * FastMath.DEG_TO_RAD, Vector3f.UNIT_X);

            bank.multLocal(pitch);
            planeNode.setLocalRotation(bank.multLocal(lookForward));

           planeNode.move(-(bankAngle / turnSpeed) * tpf,
                    -(pitchAngle / planeSpeed) * tpf, planeSpeed * tpf);
    }

    @Override
    public void simpleRender(RenderManager rm) {
        //TODO: add render code
    }

    private void initSound() {
        int volume = 0;
        if(soundEnabled){
            volume = 1;
        }
        

        audio_target = new AudioNode(assetManager, "Sounds/target.wav", false);
        audio_target.setPositional(false);
        audio_target.setLooping(false);
        audio_target.setVolume(volume);
        rootNode.attachChild(audio_target);

        audio_rotor = new AudioNode(assetManager, "Sounds/rotor.wav", false);
        audio_rotor.setPositional(false);
        audio_rotor.setLooping(true);
        audio_rotor.setVolume(volume);

        audio_pause = new AudioNode(assetManager, "Sounds/pause.wav", false);
        audio_pause.setPositional(false);
        audio_pause.setLooping(false);
        audio_pause.setVolume(volume);
        rootNode.attachChild(audio_pause);
        


    }

    public void playButtonSound() {
        audio_pause.playInstance();
    }

    private void initCamera() {

        //Disable the default flyby cam
//        flyCam.setEnabled(false);
//        // Enable a chase cam for this target (typically the player).
//        
//        ChaseCamera chaseCam = new ChaseCamera(cam, planeNode, inputManager);
//        
//        chaseCam.setSmoothMotion(true);

        // Disable the default flyby cam
        //flyCam.setEnabled(false);
        //create the camera Node
        CameraNode camNode = new CameraNode("Camera Node", cam);
        //This mode means that camera copies the movements of the target:
        camNode.setControlDir(CameraControl.ControlDirection.SpatialToCamera);
        //Attach the camNode to the target:
        planeNode.attachChild(camNode);
        //Move camNode, e.g. behind and above the target:
        camNode.setLocalTranslation(new Vector3f(2, 5, -15));
        //Rotate the camNode to look at the target:
        camNode.lookAt(planeNode.getLocalTranslation(), Vector3f.UNIT_Y);


    }

    public void onAnalog(String name, float value, float tpf) {
//        logger.log(Level.INFO, "onAnalog for {0}, value: {1}, tpf: {2}",
//                new Object[]{name, value, tpf});
        if(acceleratorEnabled){

            if (name.equalsIgnoreCase(ORIENTATION_Y_PLUS)) {
                bankAngle -= rotationSpeedY * tpf;
                

            }
            if (name.equalsIgnoreCase(ORIENTATION_Y_MINUS)) {
                bankAngle += rotationSpeedY * tpf;
                

            }
            
//        if (name.equalsIgnoreCase(ORIENTATION_X_PLUS)) {
//            pitchAngle -= rotationSpeedX *tpf;
//           
//            
//        }
//        
//        if (name.equalsIgnoreCase(ORIENTATION_X_MINUS)) {
//            pitchAngle += rotationSpeedX *tpf;
//            
//        }
//        if (name.equalsIgnoreCase(ORIENTATION_X_PLUS)) {
//            if (useAbsolute) {
//// set rotation amount
//// divide by tpf to get back to actual axis value (0 to 1)
//// multiply by 90deg so that 90deg = full axis (value = tpf)
//                anglesCurrent[1] = (scaledValue / tpf * FastMath.HALF_PI);
//            } else {
//// apply an incremental rotation amount based on rotationSpeed
//                anglesCurrent[1] += scaledValue * rotationSpeedY;
//            }
//        }
//        if (name.equalsIgnoreCase(ORIENTATION_Y_MINUS)) {
//            if (useAbsolute) {
//// set rotation amount
//// divide by tpf to get back to actual axis value (0 to 1)
//// multiply by 90deg so that 90deg = full axis (value = tpf)
//                anglesCurrent[1] = (-scaledValue / tpf * FastMath.HALF_PI);
//            } else {
//// apply an incremental rotation amount based on rotationSpeed
//                anglesCurrent[1] -= scaledValue * rotationSpeedY;
//            }
//        }
        }
    }

    private void checkCollision() {
        CollisionResults results = new CollisionResults();

        maze.collideWith(planeSpace, results);

        for (int i = 0; i < results.size(); i++) {

            String name = results.getCollision(i).getGeometry().getName();

            score++;
            audio_target.playInstance();
            //System.out.println("Collision with: "+name+" Score: "+score );

            maze.detachChildNamed(name);
        }

    }

    public int getScore() {
        return score;
    }
    
    public float getBankAngle(){
        return bankAngle;
    }
    
    public void setBankAngel(float bankAngle) {
        this.bankAngle = bankAngle;
    }

    public float getPitchAngle() {
        return pitchAngle;
    }

    public void setPitchAngle(float pitchAngle) {
        this.pitchAngle = pitchAngle;
    }
    
    
    
}
