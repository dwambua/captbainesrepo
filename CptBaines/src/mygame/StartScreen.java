/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package mygame;

import com.jme3.app.Application;
import com.jme3.app.state.AbstractAppState;
import com.jme3.app.state.AppStateManager;
import com.jme3.input.event.MouseButtonEvent;
import com.jme3.math.Vector2f;
import tonegod.gui.controls.buttons.ButtonAdapter;
import tonegod.gui.controls.windows.AlertBox;
import tonegod.gui.controls.windows.Panel;
import tonegod.gui.core.Screen;

/**
 * Manages all the gui stuff before the user starts the game
 * @author dennis
 */
public class StartScreen extends AbstractAppState {
    private Screen screen;
    private Main m;
    private Panel win;
    private boolean start = false;
   
    

    public StartScreen(Screen screen, Main m) {
        this.screen  = screen;
        this.m = m;
    }
    
    private void startGame(){
        start = true;
    }
    
    
    @Override
    public void initialize(AppStateManager stateManager, Application app) {
        super.initialize(stateManager, app);
        //TODO: initialize your AppState, e.g. attach spatials to rootNode
        //this is called on the OpenGL thread after the AppState has been attached
        
        
        // Add window
        win = new Panel(screen, new Vector2f((screen.getWidth() / 2)-(350/2),
                screen.getHeight() / 3), new Vector2f(350, 210));

        // create button and add to window
        ButtonAdapter start = new ButtonAdapter(screen, new Vector2f((win.getWidth()/2)-100, 10),
                new Vector2f(200, 50)) {
            @Override
            public void onButtonMouseLeftUp(MouseButtonEvent evt, boolean toggled) {
               startGame();
               m.playButtonSound();
            }
        };
        ButtonAdapter button = new ButtonAdapter(screen, new Vector2f((win.getWidth()/2)-100, 80),
                new Vector2f(200, 50)) {
            @Override
            public void onButtonMouseLeftUp(MouseButtonEvent evt, boolean toggled) {

                showAboutDialog();
                m.playButtonSound();
            }
        };
        ButtonAdapter exit = new ButtonAdapter(screen, new Vector2f((win.getWidth()/2)-100, 140),
                new Vector2f(200, 50)) {
            @Override
            public void onButtonMouseLeftUp(MouseButtonEvent evt, boolean toggled) {

                app.stop();
                m.playButtonSound();
            }
        };

        start.setButtonIcon(200, 50, "Interface/PLAY.png");
        
        button.setButtonIcon(200, 50, "Interface/ABOUT.png");
        exit.setButtonIcon(200, 50, "Interface/EXIT.png");
        // Add it to our initial window
        
        
        
        win.setIsMovable(false);
        win.setIsResizable(false);
        win.addChild(exit);
        win.addChild(button);
        win.addChild(start);


        // Add window to the screen
        screen.addElement(win);
    }
    
    @Override
    public void update(float tpf) {
        //TODO: implement behavior during runtime
        if(start == true){
            m.startGame();
        }
    }
    
    @Override
    public void cleanup() {
        super.cleanup();
        //TODO: clean up what you initialized in the initialize method,
        //e.g. remove all spatials from rootNode
        //this is called on the OpenGL thread after the AppState has been detached
        start  = false;
        screen.removeElement(win);
        
    }
    

    public final void showAboutDialog() {
        AlertBox aboutDialog = new AlertBox(screen, "aboutDialog", new Vector2f(15, 15),
                new Vector2f(400, 300)) {
            @Override
            public void onButtonOkPressed(MouseButtonEvent evt, boolean toggled) {
                screen.removeElement(this);
            }
        };
        aboutDialog.setMsg("Just fly the plane");
        screen.addElement(aboutDialog);

    }
}
